## full_E7533-user 11 RP1A.200720.011 mp1k61v164bspP6 release-keys
- Manufacturer: micromax
- Platform: mt6765
- Codename: E7533
- Brand: Micromax
- Flavor: full_E7533-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: mp1k61v164bspP6
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-IN
- Screen Density: 280
- Fingerprint: Micromax/E7533/E7533:11/RP1A.200720.011/mp1k61v164bspP6:user/release-keys
- OTA version: 
- Branch: full_E7533-user-11-RP1A.200720.011-mp1k61v164bspP6-release-keys
- Repo: micromax_e7533_dump_28114


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
